<?php
include '../../vendor/autoload.php';
use App\Voting\Voting;

$obj = new Voting;

if ($_POST['candidate_id']=='') {
	echo "<script>alert('Please select One Candidate!');location.href='../../view/vote_completed.php'</script>";
}else
{
	$vote_data=$obj->is_submit_vote($_POST['voter_id']);
	if($vote_data)
	{
		echo "<script>alert('Your Allready  Voted');location.href='../../view/vote_completed.php'</script>";
	}else{
		$obj->setData($_POST);
		$obj->store();
	}
}
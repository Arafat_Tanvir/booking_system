<?php

namespace App\Voting;
use App\Database;
use PDO;

class Voting extends Database
{
	protected $vote_id;
	protected $voter_id;
	protected $center_id;
	protected $candidate_id;
	protected $vote_count;
	protected $created_at;

	public function setData($data){

		if(array_key_exists('vote_id', $data))
            $this->vote_id = $data['vote_id'];

        if(array_key_exists('voter_id', $data))
            $this->voter_id = $data['voter_id'];

		if(array_key_exists('center_id', $data))
            $this->center_id = $data['center_id'];

        if(array_key_exists('candidate_id', $data))
            $this->candidate_id = $data['candidate_id'];

        $this->vote_count = 1;

        if(array_key_exists('created_at', $data))
            $this->created_at = $data['created_at'];
        return $this;
	}

	public function index()
	{
		$query = "SELECT * from vote";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function store()
	{
		$query = "INSERT into vote (voter_id, center_id, candidate_id, vote_count, created_at) Values( ?, ?, ?, ?, ?)";
		$array_data = array($this->voter_id, $this->center_id, $this->candidate_id, $this->vote_count, $this->created_at);
		$statement= $this->db->prepare($query);
		$result = $statement->execute($array_data);
		if($result){
			echo "<script>alert('Vote Created Successfully');location.href='voter_index.php'</script>";
		}else{
			echo "<script>alert('Voter is not Created');location.href='voter_create.php'</script>";
		}

	}

	public function is_submit_vote($voter_id)
	{
		$query = "SELECT * from vote where voter_id=".$voter_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function get_all_custed($voter_id)
	{
		$query = "SELECT * from vote where voter_id=".$voter_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function get_all_vote_submited_BycenterId($center_id)
	{
		$query = "SELECT * from vote where center_id=".$center_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$allVotes = $sth->fetchAll();
		return $allVotes;
	}

	public function get_all_vote_submited_BycandidateId($candidate_id)
	{
		$query = "SELECT * from vote where candidate_id=".$candidate_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$allVotes = $sth->fetchAll();
		return $allVotes;
	}

	
}


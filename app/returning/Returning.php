<?php
namespace App\Returning;
use App\Database;
use PDO;
use App\User\User;
use App\User\Auth;

class Returning extends Database
{
	protected $returning_id;
	protected $returning_name;

	public function setData($data){

		if(array_key_exists('returning_id', $data))
			$this->returning_id = $data['returning_id'];

		if(array_key_exists('returning_name', $data))
			$this->returning_name = $data['returning_name'];

		
	}

	public function index()
	{
		$query = "SELECT * from returning_officers";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function store()
	{
		$auth= new Auth();
		$status= $auth->setData($_POST)->is_exist();
		if($status){
			echo "<script>alert('Email already exist!please Login');location.href='../../view/admin/polling_create.php'</script>";
		}else{
			$query = "INSERT into returning_officers (returning_name) Values(?)";
			$array_data = array($this->returning_name);
			$statement= $this->db->prepare($query);
			$result = $statement->execute($array_data);
			$id = $this->db->lastInsertId();
			if($result){
				$user = new User();
				$_POST['checked_by_id']=$id;
				$user->setData($_POST);
				$user->store();
				echo "<script>alert('Rolling Created Successfully');location.href='../../view/admin/returning_index.php'</script>";
			}else{

				echo "<script>alert('Returning is not Created');location.href='../../view/admin/returning_create.php'</script>";
			}
		}
	}

	public function edit()
	{
		$query = "SELECT * from returning_officers where returning_id=".$this->returning_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE returning_officers set returning_name=? where returning_id=".$this->returning_id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->returning_name);

		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('returning Updated Successfully');location.href='../../view/admin/returning_index.php'</script>";
		}else{
			echo "<script>alert('returning is not Updated');location.href='../../view/admin/returning_edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from returning_officers where returning_id=".$this->returning_id;
		$result = $this->db->query($query);
	}

}


<?php
namespace App\Seat;
use App\Database;
use PDO;

class Seat extends Database
{
	protected $seat_id;
	protected $seat_name;
	protected $seat_returning_id;

	public function setData($data)
	{
		if(array_key_exists('seat_id', $data))
			$this->seat_id = $data['seat_id'];

		if(array_key_exists('seat_name', $data))
			$this->seat_name = $data['seat_name'];


		if(array_key_exists('seat_returning_id', $data))
			$this->seat_returning_id = $data['seat_returning_id'];

	}
	public function index()
	{
		$query = "SELECT * from seats";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}
	public function store()
	{
		$query = "INSERT into seats (seat_name,seat_returning_id) Values(?,?)";
		$array_data = array($this->seat_name,$this->seat_returning_id);
		$statement= $this->db->prepare($query);
		$result = $statement->execute($array_data);
		if($result){
			echo "<script>alert('Seat Created Successfully');location.href='../../view/admin/seat_index.php'</script>";
		}else{
			echo "<script>alert('Seat is not Created');location.href='../../view/admin/seat_create.php'</script>";
		}
	}

	public function edit()
	{
		$query = "SELECT * from seats where seat_id=".$this->seat_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function search_name_byId($id)
	{
		$query = "SELECT * from seats where seat_id=".$id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE seats set seat_name=?,seat_returning_id=? where seat_id=".$this->seat_id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->seat_name,$this->seat_returning_id);
		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('Seat Updated Successfully');location.href='../../view/admin/seat_index.php'</script>";
		}else{
			echo "<script>alert('Seat is not Updated');location.href='../../view/admin/seat_edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from seats where seat_id=".$this->seat_id;
		$result = $this->db->query($query);
	}

	


	public function get_seat_byreturningofferId($returning_id)
	{
		$query = "SELECT * from seats where seat_returning_id=".$returning_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

}


<?php
namespace App\Candidate;
use App\Database;
use PDO;

class Candidate extends Database
{
	protected $candidate_id;
	protected $candidate_name;
	protected $candidate_gender;
	protected $candidate_education;
	protected $candidate_protik;
	protected $candidate_image;
	protected $candidate_seat_id;
	protected $candidate_address;
	

	public function setData($data){
		if(array_key_exists('candidate_id', $data))
			$this->candidate_id = $data['candidate_id'];

		if(array_key_exists('candidate_name', $data))
			$this->candidate_name = $data['candidate_name'];

		if(array_key_exists('candidate_seat_id', $data))
			$this->candidate_seat_id = $data['candidate_seat_id'];

		if(array_key_exists('candidate_address', $data))
			$this->candidate_address = $data['candidate_address'];

		if(array_key_exists('candidate_gender', $data))
			$this->candidate_gender = $data['candidate_gender'];

		if(array_key_exists('candidate_education', $data))
			$this->candidate_education = $data['candidate_education'];

		if(array_key_exists('candidate_protik', $data))
			$this->candidate_protik = $data['candidate_protik'];

		if(array_key_exists('candidate_image', $data))
			$this->candidate_image = $data['candidate_image'];

		
	}

	public function index()
	{
		$query = "SELECT * from candidates";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function store()
	{
		
		$query = "INSERT into candidates (candidate_name,candidate_gender,candidate_education,candidate_protik,candidate_image,candidate_seat_id,candidate_address) Values(?,?,?,?,?,?,?)";
		$array_data = array($this->candidate_name,$this->candidate_gender,$this->candidate_education,$this->candidate_protik,$this->candidate_image,$this->candidate_seat_id,$this->candidate_address);
		$statement= $this->db->prepare($query);

		$result = $statement->execute($array_data);
		if($result){
			echo "<script>alert('Candidate Created Successfully');location.href='../../view/admin/candidate_index.php'</script>";
		}else{
			echo "<script>alert('Candidate is not Created');location.href='../../view/admin/candidate_create.php'</script>";
		}

	}

	public function edit()
	{
		$query = "SELECT * from candidates where candidate_id=".$this->candidate_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE candidates set candidate_name=?,candidate_gender=?,candidate_education=?,candidate_protik=?,candidate_image=?,candidate_seat_id=?,candidate_address=? where candidate_id=".$this->candidate_id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->candidate_name,$this->candidate_gender,$this->candidate_education,$this->candidate_protik,$this->candidate_image,$this->candidate_seat_id,$this->candidate_address);

		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('Candidate Updated Successfully');location.href='../../view/admin/candidate_index.php'</script>";
		}else{
			echo "<script>alert('Candidate is not Updated');location.href='../../view/admin/candidate_edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from candidates where candidate_id=".$this->candidate_id;
		$result = $this->db->query($query);
		/*if($result){
			echo "<script>alert('candidate Deleted Successfully');location.href='../views/index.php'</script>";
		}else{
			echo "<script>alert('candidate is not Deleted');location.href='../views/index.php'</script>";
		}
*/	}

	public function get_allcandidate_byseatId($seat_id)
	{
		$query = "SELECT * from candidates where candidate_seat_id=".$seat_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

}


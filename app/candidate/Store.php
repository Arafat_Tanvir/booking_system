<?php
include '../../vendor/autoload.php';
use App\Candidate\Candidate;

$obj = new Candidate();
if(isset( $_FILES["candidate_protik"] ) && !empty( $_FILES["candidate_protik"]["name"] ) ){
	$name = time().$_FILES['candidate_protik']['name'];
	$tmplocation = $_FILES['candidate_protik']['tmp_name'];
	$current_location = "../../assets/candidates/protiks/".$name;
	move_uploaded_file($tmplocation, $current_location);
	$_POST['candidate_protik'] = $current_location;

}

if(isset( $_FILES["candidate_image"] ) && !empty( $_FILES["candidate_image"]["name"] ) ){
	$name = time().$_FILES['candidate_image']['name'];
	$tmplocation = $_FILES['candidate_image']['tmp_name'];
	$current_location = "../../assets/candidates/images/".$name;
	move_uploaded_file($tmplocation, $current_location);
	$_POST['candidate_image'] = $current_location;
}
$obj->setData($_POST);
$obj->store();

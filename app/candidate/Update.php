<?php

include '../../vendor/autoload.php';

use App\Candidate\Candidate;

$obj = new Candidate();
$obj->setData($_POST);
$singleData = $obj->edit();

$candidate_current_image = $singleData->candidate_image;
$candidate_current_protik = $singleData->candidate_protik;

if(!empty($_FILES['candidate_image']['name'])){
	$name = time().$_FILES['candidate_image']['name'];
	$tmplocation = $_FILES['candidate_image']['tmp_name'];
	$current_location = "../../assets/candidates/images/".$name;
	move_uploaded_file($tmplocation, $current_location);	
	$_POST['candidate_image'] = $current_location;
}else{
	$_POST['candidate_image'] = $candidate_current_image;
}

if(!empty($_FILES['candidate_protik']['name'])){
	$name = time().$_FILES['candidate_protik']['name'];
	$tmplocation = $_FILES['candidate_protik']['tmp_name'];
	$current_location = "../../assets/candidates/protiks/".$name;
	move_uploaded_file($tmplocation, $current_location);	
	$_POST['candidate_protik'] = $current_location;
}else{
	$_POST['candidate_protik'] = $candidate_current_protik;
}

$obj->setData($_POST);
$obj->update();

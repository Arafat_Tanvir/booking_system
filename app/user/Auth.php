<?php
namespace App\User;
if(!isset($_SESSION) )  session_start();
use App\Database as DB;
use PDO;

class Auth extends DB{

    public $email = "";
    public $password = "";

    public function __construct(){
        parent::__construct();
    }

    public function setData($data = Array()){
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)){
            $this->password = md5($data['password']);
        }

         if (array_key_exists('type', $data)) {
            $this->type = $data['type'];
        }
        return $this;
    }

    public function is_exist(){
        $query= "SELECT * FROM `users` WHERE `email`=:email";
        $result=$this->db->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $count = $result->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

     public function is_registered(){
        $query = "SELECT * FROM `users` WHERE `email`=:email AND `password`=:password";
        $result=$this->db->prepare($query);
        $adata = array(':email'=>$this->email, ':password'=>$this->password);
        $result->execute($adata);
        $result->setFetchMode(PDO::FETCH_OBJ);
        $type = $result->fetch();
        $count = $result->rowCount();

        if ($count > 0) {
            return $type;
        } else {
            return FALSE;
        }
    }

    public function logged_in(){
        if (((array_key_exists('email',$_SESSION)) && (!empty($_SESSION['email']))) && ((array_key_exists('type',$_SESSION)) && (!empty($_SESSION['type'])))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
}

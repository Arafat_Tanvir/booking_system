<?php
namespace App\User;
use App\Database as DB;
use PDO;


class User extends DB{
    public $id="";
    public $email="";
    public $password="";
    public $status="";
    public $type="";
    public $checked_by_id="";
    public $created_at="";
    public $updated_at="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){

    	 if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('status',$data)){
            $this->status=$data['status'];
        }

        if(array_key_exists('checked_by_id',$data)){
            $this->checked_by_id=$data['checked_by_id'];
        }
        if(array_key_exists('type',$data)){
            $this->type=$data['type'];
        }
        if(array_key_exists('created_at',$data)){
            $this->created_at=date('y-m-d-h-i-s');
        }
        if(array_key_exists('updated_at',$data)){
            $this->updated_at=date('y-m-d-h-i-s');
        }
        return $this;
    }

    public function store(){
        $query = "INSERT into users (email,password,status,type,checked_by_id,created_at,updated_at) Values(?,?,?,?,?,?,?)";
        $data = array($this->email,$this->password,$this->status,$this->type,$this->checked_by_id,$this->created_at,$this->updated_at);
        $sth = $this->db->prepare($query);
        $status = $sth->execute($data);
        if($status && $this->type=='admin'){
            echo "<script>alert('Registration successfull! You can login now');location.href='../login.php'</script>";
        }
    }

    public function change_password(){
        $query="UPDATE `bdms_database`.`bdms_user` SET `password`=:password  WHERE `bdms_user`.`email` =:email";
        $result=$this->db->prepare($query);
        $result->execute(array($this->password));
        //$result->execute(array($this->password));
        //$result->execute(array(':password'=>$this->password,':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
            //                                                                                                                  Utility::redirect('../../../../views/User/Profile/signUp.php');
        }
        else {
            echo "Error";
        }

    }

    public function view(){
        $query="SELECT * FROM `users` WHERE `users`.`email` =:email";
        $result=$this->db->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $allData=$result->fetch(PDO::FETCH_OBJ);
        return $allData;
    }// end of view()


    

    public function update(){
    }

    public function get_user_by_Id($id)
    {
        $query = "SELECT * from users where id=".$id;
        $sth= $this->db->query($query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

}

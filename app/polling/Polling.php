<?php
namespace App\Polling;
use App\Database;
use PDO;
use App\User\User;
use App\User\Auth;

class Polling extends Database
{
	protected $polling_id;
	protected $polling_name;

	public function setData($data){

		if(array_key_exists('polling_id', $data))
			$this->polling_id = $data['polling_id'];

		if(array_key_exists('polling_name', $data))
			$this->polling_name = $data['polling_name'];

	}

	public function index()
	{
		$query = "SELECT * from polling_officers";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function store()
	{
		$auth= new Auth();
		$status= $auth->setData($_POST)->is_exist();
		if($status){
			echo "<script>alert('Please Enter Valid Your Email');location.href='../../view/admin/polling_create.php'</script>";
		}else{
			
			$query = "INSERT into polling_officers (polling_name) Values(?)";
			$array_data = array($this->polling_name);
			$statement= $this->db->prepare($query);
			$result = $statement->execute($array_data);
			$id = $this->db->lastInsertId();
			if($result){
				$user = new User();
				$_POST['checked_by_id']=$id;
				$user->setData($_POST);
				$user->store();
				echo "<script>alert('Polling Created Successfully');location.href='../../view/admin/polling_index.php'</script>";
			}else{
				echo "<script>alert('Polling is not Created');location.href='../../view/admin/polling_create.php'</script>";
			}
		}
	}

	public function edit()
	{
		$query = "SELECT * from polling_officers where polling_id=".$this->polling_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE polling_officers set polling_name=? where polling_id=".$this->polling_id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->polling_name);

		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('polling Updated Successfully');location.href='../../view/admin/polling_index.php'</script>";
		}else{
			echo "<script>alert('polling is not Updated');location.href='../../view/admin/polling_edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from polling_officers where polling_id=".$this->polling_id;
		$result = $this->db->query($query);
	}

}


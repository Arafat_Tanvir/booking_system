<?php

namespace App\Voter;
use App\Database;
use PDO;

class Voter extends Database
{
	protected $voter_id;
	protected $voter_name;
	protected $voter_father_name;
	protected $voter_mother_name;
	protected $voter_date_of_birth;
	protected $voter_gender;
	protected $voter_image;
	protected $voter_address;
	protected $voter_national_id;
	protected $center_id;

	public function setData($data){
		if(array_key_exists('voter_id', $data))
			$this->voter_id = $data['voter_id'];

		if(array_key_exists('voter_name', $data))
			$this->voter_name = $data['voter_name'];

		if(array_key_exists('voter_father_name', $data))
			$this->voter_father_name = $data['voter_father_name'];

		if(array_key_exists('voter_mother_name', $data))
			$this->voter_mother_name = $data['voter_mother_name'];

		if(array_key_exists('voter_date_of_birth', $data))
			$this->voter_date_of_birth = $data['voter_date_of_birth'];

		if(array_key_exists('voter_image', $data))
			$this->voter_image = $data['voter_image'];

		if(array_key_exists('voter_gender', $data))
			$this->voter_gender = $data['voter_gender'];

		if(array_key_exists('voter_address', $data))
			$this->voter_address = $data['voter_address'];

		if(array_key_exists('voter_national_id', $data))
			$this->voter_national_id = $data['voter_national_id'];
		if(array_key_exists('center_id', $data))
			$this->center_id = $data['center_id'];
	}

	public function index()
	{
		$query = "SELECT * from voters";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function genatrate_national_id()
	{
		$voter_national_id=1000;
		$query = "SELECT MAX(voter_national_id) as voter_n_id FROM voters";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		
		if(count($alldata)>0){
			$voter_national_id=$alldata[0]->voter_n_id+1;
		}
		return $voter_national_id;
	}

	public function store()
	{
		$query = "INSERT into voters (voter_name, voter_father_name, voter_mother_name, voter_date_of_birth, voter_gender, voter_address,voter_image,voter_national_id,center_id) Values( ?, ?, ?, ?, ?, ?,?,?,?)";
		$array_data = array($this->voter_name, $this->voter_father_name, $this->voter_mother_name, $this->voter_date_of_birth, $this->voter_gender, $this->voter_address,$this->voter_image,$this->voter_national_id,$this->center_id);

		$statement= $this->db->prepare($query);
		$result = $statement->execute($array_data);
		if($result){
			echo "<script>alert('Voter Created Successfully');location.href='../../view/admin/voter_index.php'</script>";
		}else{
			echo "<script>alert('Voter is not Created');location.href='../../view/admin/voter_create.php'</script>";
		}

	}

	public function edit()
	{
		$query = "SELECT * from voters where voter_id=".$this->voter_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE voters set voter_name=?,voter_father_name=?,voter_mother_name=?,voter_date_of_birth=?,voter_gender=?,voter_address=?,voter_image=? ,center_id=? where voter_id=".$this->voter_id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->voter_name, $this->voter_father_name, $this->voter_mother_name, $this->voter_date_of_birth, $this->voter_gender, $this->voter_address,$this->voter_image,$this->center_id);

		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('Voter Updated Successfully');location.href='../../view/admin/voter_index.php'</script>";
		}else{
			echo "<script>alert('Voter is not Updated');location.href='../../view/admin/voter_edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from voters where voter_id=".$this->voter_id;
		$result = $this->db->query($query);
	}

	public function get_voter_by_id($get=null)
    {
        if($get)
        {

           $voter_id = $get['voter_id'];
            $query = "SELECT * from voters where voter_id=".$voter_id;
            $sth= $this->db->query($query);
            $sth->setFetchMode(PDO::FETCH_OBJ);
            $singleData = $sth->fetch();
            return $singleData;
        }
        else
        {

            echo "<script>location.href='../../voting_system/view/blank.php'</script>";
        }

    }

    public function get_allvoters_bycenterId($center_id)
	{
		$query = "SELECT * from voters where center_id=".$center_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function product_search()
    {
        $search=$_POST['search_key'];
        $query = "SELECT * from voters where voter_national_id=".$search;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
    }

}


<?php
include '../../vendor/autoload.php';
use App\Voter\Voter;

$obj = new Voter;
$name = time().$_FILES['voter_image']['name'];
$tmplocation = $_FILES['voter_image']['tmp_name'];
$current_location = "../../assets/voters/images/".$name;
move_uploaded_file($tmplocation, $current_location);
$_POST['voter_image'] = $current_location;
$obj->setData($_POST);

$obj->store();

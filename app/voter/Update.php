<?php

include '../../vendor/autoload.php';

use App\Voter\Voter;

$obj = new Voter;
$obj->setData($_POST);
$singleData = $obj->edit();

$current_image = $singleData->voter_image; 

if(!empty($_FILES['voter_image']['name'])){
	$name = time().$_FILES['voter_image']['name'];
	$tmplocation = $_FILES['voter_image']['tmp_name'];
	$current_location = "../../assets/voters/images/".$name;
	move_uploaded_file($tmplocation, $current_location);	
	$_POST['voter_image'] = $current_location;
}else{
	$_POST['voter_image'] = $current_image;
}
$obj->setData($_POST);
$obj->update();

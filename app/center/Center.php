<?php
namespace App\Center;
use App\Database;
use PDO;

class Center extends Database
{
	protected $center_id;
	protected $center_name;
	protected $center_seat_id;
	protected $center_address;

	public function setData($data){
		if(array_key_exists('center_id', $data))
			$this->center_id = $data['center_id'];

		if(array_key_exists('center_name', $data))
			$this->center_name = $data['center_name'];

		if(array_key_exists('center_seat_id', $data))
			$this->center_seat_id = $data['center_seat_id'];

		if(array_key_exists('center_address', $data))
			$this->center_address = $data['center_address'];
		if(array_key_exists('center_polling_id', $data))
			$this->center_polling_id = $data['center_polling_id'];
	}

	public function index()
	{
		$query = "SELECT * from centers";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function store()
	{
		
		$query = "INSERT into centers (center_name,center_seat_id,center_address,center_polling_id) Values(?,?,?,?)";
		$array_data = array($this->center_name,$this->center_seat_id,$this->center_address,$this->center_polling_id);
		$statement= $this->db->prepare($query);
		$result = $statement->execute($array_data);
		if($result){
			echo "<script>alert('Center Created Successfully');location.href='../../view/admin/center_index.php'</script>";
		}else{
			echo "<script>alert('Center is not Created');location.href='../../view/admin/center_create.php'</script>";
		}

	}

	public function search_name_byId($id)
	{
		$query = "SELECT * from centers where center_id=".$id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function edit()
	{
		$query = "SELECT * from centers where center_id=".$this->center_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE centers set center_name=?,center_seat_id=?,center_address=?,center_polling_id=? where center_id=".$this->center_id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->center_name,$this->center_seat_id,$this->center_address,$this->center_polling_id);

		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('Center Updated Successfully');location.href='../../view/admin/center_index.php'</script>";
		}else{
			echo "<script>alert('Center is not Updated');location.href='../../view/admin/center_edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from centers where center_id=".$this->center_id;
		$result = $this->db->query($query);
		/*if($result){
			echo "<script>alert('center Deleted Successfully');location.href='../views/index.php'</script>";
		}else{
			echo "<script>alert('center is not Deleted');location.href='../views/index.php'</script>";
		}
*/	}

	public function get_allcenter_byseatId($seat_id)
	{
		$query = "SELECT * from centers where center_seat_id=".$seat_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function get_center_bypollingofficerId($polling_id)
	{
		$query = "SELECT * from centers where center_polling_id=".$polling_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singledata = $sth->fetch();
		return $singledata;
	}

	public function get_center_by_id($center_id)
	{
		$query = "SELECT * from centers where center_id=".$center_id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singledata = $sth->fetch();
		return $singledata;
	}
}


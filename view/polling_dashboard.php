<?php
include '../vendor/autoload.php';
if(!isset($_SESSION) )session_start();

use App\Voter\Voter;
use App\Voting\Voting;
use App\Returning\Returning;
use App\Candidate\Candidate;
use App\Seat\Seat;
use App\Center\Center;
use App\User\Auth;

if($_SESSION['type']=='polling'){

}else{
  header("location:login_new.php");
}

$obj_center=new Center();
$center=$obj_center->get_center_bypollingofficerId($_SESSION['checked_by_id']);

//this center Total voters
$obj_voter= new Voter();
$total_voters_this_center=$obj_voter->get_allvoters_bycenterId($center->center_id);

//this center submit vote
$obj_voting=new Voting();
$total_cust_vote_this_center=$obj_voting->get_all_vote_submited_BycenterId($center->center_id);
//total Un Cust vote
$un_coust_vote_this_ceter=(count($total_voters_this_center)-count($total_cust_vote_this_center));

$obj=new Voter();
if (empty($_POST)) {
  
}else{
    $voter=$obj->product_search($_POST);
   
}



$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    header("location:../login.php");
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voter Panel</title>
    <link href="../assets/admin_lte_files/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container" style="margin-top: 40px;">
         <div class="card-header">
                         <h4> Checked Voter Valid</h4>

                    </div>
             
        <br>
        <a class="badge badge-warning float-right" href="user/logout.php">Logout</a>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <form action="polling_dashboard.php" method="POST">
                          <div class="form-group">
                            <label for="search_key">Is Voter Id Valid?</label>
                            <input type="number" class="form-control" name="search_key" id="search_key" placeholder="Enter Voter Id" required>
                          </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
      <?php if (!empty($voter)) { ?>

        <div class="row">
           <div class="col-sm-8">
           
                <div class="card">
                    <div class="card-header">
                         <h2 style="color: #aaabbb;size: 30px;text-align: center;">Voter Information</h2>
                        <table class="table table-bordered table-striped">
                            <thead>
                                 <tr style="background-color: green;color: white">
                                    <th>Voter National ID</th>
                                    <td><?php echo $voter->voter_national_id ?></td>
                                </tr>
                                <tr>
                                    <th>Voter Name</th>
                                    <td><?php echo $voter->voter_name ?></td>
                                </tr>
                                <tr>
                                    <th>Father Name</th>
                                    <td><?php echo $voter->voter_father_name ?></td>
                                </tr>

                                <tr>
                                    <th>Mother Name</th>
                                    <td><?php echo $voter->voter_mother_name ?></td>
                                </tr>

                                <tr>
                                    <th>Date Of Birth</th>
                                    <td><?php echo $voter->voter_date_of_birth ?></td>
                                </tr>
                               
                                <tr>
                                    <th>Voter Gender</th>
                                    <td><?php echo $voter->voter_gender ?></td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
              </div>
        </div>

        <?php } else if(!empty($_POST) && empty($voter)) { ?>
     
            <div class="row">
               <div class="col-sm-8">
                    <div class="alert alert-danger">
                        Invalid Voter National ID
                    </div>
                </div>
            </div>

        <?php } ?>
    

    </div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
window.onload = function() {

var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title: {
        text: "Election Bangladesh 2020"
    },
    data: [{
        type: "pie",
        startAngle: 240,
        // yValueFormatString: "##0.00\"%\"",
        indexLabel: "{label} {y}",
        dataPoints: [
            {y: <?= count($total_cust_vote_this_center) ?>, label: "Vote Cast"},
            {y: <?= $un_coust_vote_this_ceter ?>, label: "Vote Not Cast"},
            {y: <?= count($total_voters_this_center) ?>, label: "Total Vote"}
        ]
    }]
});
chart.render();

}
</script>
</body>
</html>
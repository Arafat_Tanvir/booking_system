<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\User\Auth;

$auth= new Auth();
$auth->setData($_POST);
$status= $auth->is_registered();

if($status){

    $_SESSION['email']=$status->email;
    $_SESSION['type']=$status->type;
    $_SESSION['id']=$status->id;
    $_SESSION['checked_by_id']=$status->checked_by_id;
    

    if($status->type=='admin')
    {
        echo "<script>location.href='../admin/dashboard.php'</script>";
    }
    elseif ($status->type=='polling')
    {
       echo "<script>location.href='../polling_dashboard.php'</script>";
    }
    elseif ($status->type=='returning')
    {
        echo "<script>location.href='../returning_dashboard.php'</script>";
    }


}else{
    $_SESSION['wrong_crd']='Invalid Email Or Password';
	//header('location: ../index.php');
    echo "<script>alert('Invalid Email Or Password');location.href='../login_new.php'</script>";
    //echo "<script>location.href='login.php'</script>";
}

<?php
include '../vendor/autoload.php';
if(!isset($_SESSION) )session_start();

use App\Voter\Voter;
use App\Center\Center;
use App\Candidate\Candidate;
use App\User\Auth;



$obj= new Candidate();


$obj_center=new Center;

$voter=new Voter();
$voter_info=$voter->get_voter_by_id($_GET);

$center=$obj_center->get_center_by_id($voter_info->center_id);
$candidates = $obj->get_allcandidate_byseatId($center->center_seat_id);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Voter Panel</title>
    <link href="../assets/admin_lte_files/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        .candidate_logo{
            display: inline-block;
            width: 100px;
            height: 100px;
            border-radius: 100%;
        }
        .candidate_name{
            padding-top: 5px;

            display: inline-block;
            margin-top: 20px;
            font-size: 20px;
            font-family: sans-serif;
            font-weight: bolder;
        }
    </style>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="alert alert-success text-center" role="alert">
               <h2>Voting System</h2>
            </div>
        </div>

    </div>
    <div class="row mb-2">
        <div class="col-md-6">
            <div class="card" style="">
                <img src="voting_system/<?php echo $voter_info->voter_image;?>" class="card-img-top" height="300" alt="...">
                <div class="card-body">
                    <?php
                    $voter_info->voter_image;
                    ?>
                    <h5 class="card-title">Voter ID : <?= $voter_info->voter_national_id?></h5>

                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Name : <?= $voter_info->voter_name?></li>
                    <li class="list-group-item">Father Name : <?= $voter_info->voter_father_name?></li>
                    <li class="list-group-item">Mother Name : <?= $voter_info->voter_mother_name?></li>
                    <li class="list-group-item">Date OF Birth : <?= $voter_info->voter_date_of_birth?></li>
                    <li class="list-group-item">Gander : <?= $voter_info->voter_gender?></li>
                    <li class="list-group-item"><p class="card-text">Voter Address :<?= $voter_info->voter_address?></p></li>
                </ul>

            </div>
        </div>
        <div class="col-md-6 text-center">
            <form action="../app/voting/Store.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="voter_id" value='<?= $voter_info->voter_id?>'>
                <input type="hidden" name="center_id" value='<?= $voter_info->center_id?>'>
                <ul class="list-group list-group-flush">
                    <?php foreach ($candidates as $candidate){?>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="voting_system/<?php echo $candidate->candidate_protik;?>" class="card-img-top candidate_logo"  alt="...">
                                </div>
                                <div class="col-md-4">
                                    <span class="candidate_name"><?php echo $candidate->candidate_name;?></span>
                                </div>
                                <div class="col-md-4 pt-3">
                                    <div class="form-check">

                                        <input class="form-check-input" type="radio" name="candidate_id" id="yes" value="<?php echo $candidate->candidate_id;?>" >
                                        <label class="form-check-label" for="yes">
                                            Yes
                                        </label>
                                    </div>
                                </div>
                            </div>



                        </li>
                    <?php }?>
                </ul>
                <button type="submit" class="btn btn-danger mt-2">Submit</button>
            </form>

        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
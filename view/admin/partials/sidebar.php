<?php

include "../../vendor/autoload.php";
use App\Database;

new Database;
?>

<!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Voting<sup>2020</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Voters</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Voter Control:</h6>
            <a class="collapse-item" href="voter_create.php">Add Voter</a>
            <a class="collapse-item" href="voter_index.php">View Voter</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo_polling" aria-expanded="true" aria-controls="collapseTwo_polling">
          <i class="fas fa-fw fa-cog"></i>
          <span>Polling Officers</span>
        </a>
        <div id="collapseTwo_polling" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Polling Officer Control:</h6>
            <a class="collapse-item" href="polling_create.php">Add Polling</a>
            <a class="collapse-item" href="polling_index.php">View Polling</a>
          </div>
        </div>
      </li>

       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo_returning" aria-expanded="true" aria-controls="collapseTwo_returning">
          <i class="fas fa-fw fa-cog"></i>
          <span>Returning Officers</span>
        </a>
        <div id="collapseTwo_returning" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Returning Officer Control:</h6>
            <a class="collapse-item" href="returning_create.php">Add Returning</a>
            <a class="collapse-item" href="returning_index.php">View Returning</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Seats</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Seat Control:</h6>
            <a class="collapse-item" href="seat_create.php">Add Seat</a>
            <a class="collapse-item" href="seat_index.php">Show Seat</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilitiescenter" aria-expanded="true" aria-controls="collapseUtilitiescenter">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Centers</span>
        </a>
        <div id="collapseUtilitiescenter" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Center Control:</h6>
            <a class="collapse-item" href="center_create.php">Add Center</a>
            <a class="collapse-item" href="center_index.php">Show Center</a>
          </div>
        </div>
      </li>
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Candidates</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Candidates Control:</h6>
            <a class="collapse-item" href="candidate_create.php">Add Candidate</a>
            <a class="collapse-item" href="candidate_index.php">Show Candidate</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages_result" aria-expanded="true" aria-controls="collapsePages_result">
          <i class="fas fa-fw fa-folder"></i>
          <span>Results</span>
        </a>
        <div id="collapsePages_result" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="result.php">Show Result</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
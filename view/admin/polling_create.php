<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}


$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
include 'partials/header.php'; 
?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Polling Information</h1>
            <a href="polling_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Polling</a>
          </div>
          <div class="row justify-content-center" style="margin-bottom: 20px;">
            <div class="col-sm-8">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" action="../../app/polling/Store.php">
           <input type="hidden" name="type" value="polling">
	         <div class="form-group row">
	              <label for="polling_name" class="col-sm-4">Polling Name </label>
	              <div class="col-sm-8">
	                  <input type="text" class="form-control " name="polling_name" id="polling_name " placeholder="Enter Seat Name " value="" required>
	              </div>
	          </div>

	           <div class="form-group row">
	              <label for="email" class="col-sm-4">Polling Email </label>
	              <div class="col-sm-8">
	                  <input type="email" class="form-control " name="email" id="email " placeholder="Enter Seat Name " value="" required>
	              </div>
	          </div>

	           <div class="form-group row">
	              <label for="password" class="col-sm-4">Polling Password </label>
	              <div class="col-sm-8">
	                  <input type="password" class="form-control " name="password" id="password " placeholder="Enter Seat Name " value="" required>
	              </div>
	          </div>
          <button class="btn btn-primary" type="submit">Create</button>
      </form>
       </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>

 
<?php 

include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Polling\Polling;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}


$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
$obj= new Polling();
$obj->setData($_GET);
$polling = $obj->edit();


include 'partials/header.php'; 
?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Polling Information</h1>
            <a href="polling_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Polling</a>
          </div>
          <div class="row justify-content-center" style="margin-bottom: 20px;">
            <div class="col-sm-8 justify-content-center">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" action="../../app/polling/Update.php" enctype="multipart/form-data">
         <input type="hidden" name="polling_id" value="<?php echo $polling->polling_id; ?>">
         <div class="form-group row">
              <label for="polling_name" class="col-sm-4">Center Name </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control " name="polling_name" id="polling_name " placeholder="Enter Voter Name " value=" <?php echo $polling->polling_name; ?>">
              </div>
          </div>

          <button class="btn btn-primary float-right" type="submit">Update</button>
      </form>
       </div>
       </div>
              </div>
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>
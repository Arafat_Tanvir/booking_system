<?php 

include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Center\Center;
use App\Seat\Seat;
use App\Polling\Polling;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
$obj= new Center();
$obj->setData($_GET);
$center = $obj->edit();

//seat

$obj= new Seat();
$seats = $obj->index();

$obj_polling= new Polling();
$pollinges = $obj_polling->index();
include 'partials/header.php'; 
?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Center Information</h1>
            <a href="center_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Center</a>
          </div>
          <div class="row" style="margin-bottom: 20px;">
            <div class="col-sm-8 justify-content-center">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" action="../../app/center/Update.php" enctype="multipart/form-data">
         <input type="hidden" name="center_id" value="<?php echo $center->center_id; ?>">
         <div class="form-group row">
              <label for="center_name" class="col-sm-4">Center Name </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control " name="center_name" id="center_name " placeholder="Enter Voter Name " value=" <?php echo $center->center_name; ?>">
              </div>
          </div>

          <div class="form-group row">
	              <label for="center_seat_id" class="col-sm-4">Seat Name </label>
	              <div class="col-sm-8">
	                  <select name="center_seat_id" id="center_seat_id" class="form-control">
	                      <option value="0" disabled="true" selected="true">===Select Seat Name===</option>
	                         <?php foreach ($seats as $seat) { ?>
	                         <option value="<?php echo $seat->seat_id; ?>" <?php if($seat->seat_id==$center->center_seat_id) echo 'selected'; ?>><?php echo $seat->seat_name; ?></option>
	                         <?php } ?>
	                          
	                         
	                  </select>
	              </div>
	          </div>

            <div class="form-group row">
                <label for="center_polling_id" class="col-sm-4">Polling Officer Name </label>
                <div class="col-sm-8">
                    <select name="center_polling_id" id="center_polling_id" class="form-control">
                        <option value="0" disabled="true" selected="true">===Select Seat Name===</option>
                           <?php foreach ($pollinges as $polling) { ?>
                           <option value="<?php echo $polling->polling_id; ?>" <?php if($polling->polling_id==$center->center_polling_id) echo 'selected'; ?>><?php echo $polling->polling_name; ?></option>
                           <?php } ?>
                    </select>
                </div>
            </div>

	         
	           <div class="form-group row">
              <label for="center_address" class="col-sm-4">Center Address</label>
              <div class="col-sm-8">
                  <textarea name="center_address" cols="4" rows="5" value="" class="form-control" id="center_address"><?php echo $center->center_address; ?></textarea>
                 
              </div>
          </div>
          <button class="btn btn-primary float-right" type="submit">Update</button>
      </form>
       </div>
       </div>
              </div>
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>
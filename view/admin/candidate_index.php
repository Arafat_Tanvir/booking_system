<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();

use App\User\Auth;
use App\Candidate\Candidate;
use App\Seat\Seat;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}

//for Auth Object Create
$auth= new Auth();
$status = $auth->setdata($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
//for Candidate object create
$obj= new Candidate();
$candidates = $obj->index();


$obj_seat= new Seat();


$i= 1;
include 'partials/header.php'; 
?>


<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <?php
      include 'partials/sidebar.php' 
       ?>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">All candidate Information</h1>
            <a href="candidate_create.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Add Center</a>
          </div>

          <!-- Content Row -->
          <div class="row">

            <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sl</th>
                <th>Protik</th>

                <th>Seat</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Image</th>
                <th>Education</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($candidates as $candidate) { ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><img src="<?php echo $candidate->candidate_protik; ?>" class="img-responsive" width="50"></td>
               
                <td>
                  <?php echo $obj_seat->search_name_byId($candidate->candidate_seat_id)->seat_name ?>
                </td>
                <td><?php echo $candidate->candidate_name; ?></td>
                
                <td><?php echo $candidate->candidate_gender; ?></td>
                <td><img src="<?php echo $candidate->candidate_image; ?>" class="img-responsive" width="50"></td>
                
                <td><?php echo $candidate->candidate_education; ?></td>
                <td><?php echo $candidate->candidate_address; ?></td>
                <td>
                  <a href="candidate_edit.php?candidate_id=<?php echo $candidate->candidate_id; ?>" class="btn btn-warning btn-sm">Edit</a>
                  <a href="../../app/candidate/Delete.php?candidate_id=<?php echo $candidate->candidate_id; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure!')">Delete</a>
                </td>
              </tr>

              <?php } ?>
            </tbody>
          </table>

          
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
<?php 
 include 'partials/footer.php'; 
?>
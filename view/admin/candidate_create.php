<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Seat\Seat;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}

$obj= new Seat();
$seats = $obj->index();
include 'partials/header.php'; 
?>
		

 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Candidate Information</h1>
            <a href="candidate_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Candidate</a>
          </div>
          <div class="row justify-content-center" style="margin-bottom: 20px;">
            <div class="col-sm-8">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" id="candidateForm" action="../../app/candidate/Store.php" enctype="multipart/form-data">

            	<div class="form-group row">
	              <label for="candidate_seat_id" class="col-sm-4">Seat Name </label>
	              <div class="col-sm-8">
	                  <select name="candidate_seat_id" id="candidate_seat_id" class="form-control">
	                      <option value="0" disabled="true" selected="true">===Select Seat Name===</option>
	                         <?php foreach ($seats as $seat) { ?>
	                         <option value="<?php echo $seat->seat_id; ?>" ><?php echo $seat->seat_name; ?></option>
	                         <?php } ?>
	                         
	                  </select>
	              </div>
	          </div>

	          <div class="form-group row">
	              <label for="candidate_protik" class="col-sm-4">Candidate Protik</label>
	              <div class="col-sm-8">
	                  <input type="file" class="form-control " name="candidate_protik"  id="candidate_protik"  placeholder="Enter Ward Bangla Name" value="" required>
	              </div>
	          </div>

	         <div class="form-group row">
	              <label for="candidate_name" class="col-sm-4">Candidate Name </label>
	              <div class="col-sm-8">
	                  <input type="text" class="form-control " name="candidate_name" id="candidate_name " placeholder="Enter Seat Name " value="" required>
	              </div>
	          </div>

	          <div class="form-group row">
	              <label for="candidate_gender" class="col-sm-4">Candidate Gender</label>
	              <div class="col-sm-8">
	                  <div class="row justify-content-center" >
	                      <label class="col-sm-6">
	                          <input type="radio" name="candidate_gender" class="candidate_gender" id="candidate_gender" value="Male">Male
	                      </label>
	                      <label class="col-sm-6 ">
	                           <input type="radio" name="candidate_gender" class="candidate_gender" id="candidate_gender" value="Female">Female
	                      </label>
	                  </div>
	              </div>
	          </div>

	          <div class="form-group row">
	              <label for="candidate_education" class="col-sm-4">Candidate Education </label>
	              <div class="col-sm-8">
	                  <input type="text" class="form-control " name="candidate_education" id="candidate_education " placeholder="Enter Candidate Education " value="" required>
	              </div>
	          </div>


	          <div class="form-group row">
              <label for="candidate_image" class="col-sm-4">Candidate Image</label>
              <div class="col-sm-8">
                  <input type="file" class="form-control " name="candidate_image"  id="candidate_image"  placeholder="Enter Ward Bangla Name" value="" required>
              </div>
          </div>

	         
	           <div class="form-group row">
              <label for="candidate_address" class="col-sm-4">Candidate Address</label>
              <div class="col-sm-8">
                  <textarea name="candidate_address" cols="4" rows="5" value="" class="form-control" id="candidate_address" required></textarea>
                 
              </div>
          </div>

          <button class="btn btn-primary" type="submit">Create</button>
      </form>
       </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>

 <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script>
               $(document).ready(function () {
                   $.validator.setDefaults({
                       errorClass: "textstyle py-2 pb-1",
                       highlight: function (element, errorClass) {
                           $(element).removeClass(errorClass);
                       },
                       errorPlacement: function (error, element) {
                           error.insertAfter($(element).parent('div'));
                       }
                      

                   });
                   

                   $("#candidateForm").validate({

                       errorElement: 'div',
                       rules: {
                           candidate_name: {
                               required: true,
                              
                           },
                            candidate_seat_id: {
                               required: true,
                           },
                            candidate_protik: {
                               required: true,
                               
                           },
                            candidate_gender: {
                               required: true,
                           },
                            candidate_image: {
                               required: true,
                           }
                           ,
                            candidate_address: {
                               required: true,
                           }
                           ,
                            candidate_education: {
                               required: true,
                           }
                       },
                       messages: {
                           
                           candidate_name: {
                               required: "Enter Your Name"
                           },
                           candidate_seat_id: {
                           	   required: "Select Seat Name"
                           },
                            candidate_protik: {
                               required:"Input Protik(jpg file)"
                               
                           },
                            candidate_gender: {
                               required: "checked Gender"
                           },
                           candidate_image: {
                               required:"Input image(jpg file)"
                               
                           },
                            candidate_address: {
                               required: "Enter Your Address!"
                           }
                            ,
                            candidate_education: {
                               required: "Enter Education"
                           }
                       }
                   });
               });
           </script>

 
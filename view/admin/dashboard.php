<?php 


include '../../vendor/autoload.php';
use App\User\Auth;
use App\Seat\Seat;
use App\Center\Center;
use App\Voter\Voter;
use App\Voting\Voting;
if(!isset($_SESSION) )session_start();
if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}
$total_cast=0;
$total_voter=0;

$obj_seat=new Seat();
$seats=$obj_seat->index();


$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login_new.php");
}
$i=1;
include 'partials/header.php'; 
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <?php
      include 'partials/sidebar.php' 
       ?>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <?php 
          include 'partials/navbar.php'
           ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div>

          <!-- Content Row -->
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>National Election</h4>
                </div>
                <div class="card-body">
                  <div class="row">
          
                         <h2 style="color: #aaabbb;size: 30px;text-align: center;">Candidate Selected By Seat</h2>
                        <table class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Sl</th>
                                <th>Seat Name</th>
                                <th>Vote Chart</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach($seats as $seat) { 
                                $obj_center=new Center();
                                $obj_voting=new Voting;
                                $centers=$obj_center->get_allcenter_byseatId($seat->seat_id);
                               
                                
                                $obj_voting=new Voting();
                                $obj_voter= new Voter();
                                foreach ($centers as $center) {
                                    $total_cust_vote_this_center=$obj_voting->get_all_vote_submited_BycenterId($center->center_id);
                                    $total_cast +=count($total_cust_vote_this_center);
                                    $voters=$obj_voter->get_allvoters_bycenterId($center->center_id);
                                    $total_voter+=count($voters);
                                }
                                $total_uncast=($total_voter-$total_cast);

                                ?>
                              <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $seat->seat_name; ?></td> 
                                <td>
                                  <div class="col-sm-12">
                                      <div id="chartContainer" style="height: 200px; width: 100%;"></div>
                                  </div>
                                </td>    
                              </tr>

                              <?php } ?>
                            </tbody>
                          </table>
                    </div>
                </div>
              </div>
            </div> 
            
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
       <?php 
           include 'partials/footer.php'; 
         ?>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

 <script>
  window.onload = function() {

  var chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      title: {
          text: "Election Bangladesh 2020"
      },
      data: [{
          type: "pie",
          startAngle: 240,
          // yValueFormatString: "##0.00\"%\"",
          indexLabel: "{label} {y}",
          dataPoints: [
              {y: <?= $total_cast ?>, label: "Vote Cast"},
              {y: <?= $total_uncast ?>, label: "Vote Not Cast"},
              {y: <?= $total_voter ?>, label: "Total Vote"}
          ]
      }]
  });
  chart.render();

  }
  </script>

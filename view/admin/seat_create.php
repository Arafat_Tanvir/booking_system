<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Returning\Returning;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}


$obj=new Returning();
$returns=$obj->index();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}

include 'partials/header.php'; 

?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Seat Information</h1>
            <a href="seat_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Seat</a>
          </div>
          <div class="row justify-content-center" style="margin-bottom: 20px;">
            <div class="col-sm-8">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" action="../../app/seat/Store.php" enctype="multipart/form-data">
	         <div class="form-group row">
	              <label for="seat_name" class="col-sm-4">Seat Name </label>
	              <div class="col-sm-8">
	                  <input type="text" class="form-control " name="seat_name" id="seat_name " required placeholder="Enter Seat Name " value="" required>
	              </div>
	          </div>

            <div class="form-group row">
                <label for="seat_returning_id" class="col-sm-4">Returning Officer Name </label>
                <div class="col-sm-8">
                    <select name="seat_returning_id" id="seat_returning_id" required class="form-control">
                        <option value="0" disabled="true" selected="true">===Select Returning Officer Name===</option>
                           <?php foreach ($returns as $returning) { ?>
                           <option value="<?php echo $returning->returning_id; ?>" ><?php echo $returning->returning_name; ?></option>
                           <?php } ?>
                           
                    </select>
                </div>
            </div>

          <button class="btn btn-primary" type="submit">Create</button>
      </form>
       </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>

 
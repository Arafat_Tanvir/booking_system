<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\User;
use App\User\Auth;
use App\Voter\Voter;
use App\Center\Center;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}


$center = new Center();
$obj= new Voter();
$voters = $obj->index();

$auth= new Auth();
$status = $auth->setdata($_SESSION)->logged_in();

if(!$status) {
    header("location:../login.php");
}

$i= 1;

include 'partials/header.php'; 
?>


<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <?php
      include 'partials/sidebar.php' 
       ?>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">All Voter Information</h1>
            <a href="voter_create.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Add Voter</a>
          </div>

          <!-- Content Row -->
          <div class="row">

            <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sl</th>
                <th>National ID</th>
                <th>Name</th>
                <th>Father Name</th>
                <th>Mother Name</th>
                <th>Gender</th>
                <th>Date Of Birth</th>
                <th>Center</th>
                <th>Image</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($voters as $voter) {
                 $center_name= $center->search_name_byId($voter->center_id);
                  ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $voter->voter_name; ?></td>
                <td><?php echo $voter->voter_national_id; ?></td>
                <td><?php echo $voter->voter_father_name; ?></td>
                <td><?php echo $voter->voter_mother_name; ?></td>
                <td><?php echo $voter->voter_gender; ?></td>
                <td><?php echo $voter->voter_date_of_birth; ?></td>
                <td><?php echo ($center_name)?$center_name->center_name:''; ?></td>
                <td><img src="<?php echo $voter->voter_image; ?>" class="img-responsive" width="50"></td>
                <td><?php echo $voter->voter_address; ?></td>
                <td style="display:flex;">
                  <a href="voter_edit.php?voter_id=<?php echo $voter->voter_id; ?>" class="btn btn-warning btn-sm mr-2">Edit</a>
                  <a href="../../app/voter/Delete.php?voter_id=<?php echo $voter->voter_id; ?>" class="btn btn-danger btn-sm " onclick="return confirm('Are you sure!')">Delete</a>
                </td>
              </tr>

              <?php } ?>
            </tbody>
          </table>

          
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
<?php 
 include 'partials/footer.php'; 
?>
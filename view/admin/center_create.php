<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Seat\Seat;
use App\Polling\Polling;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}


$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}

$obj= new Seat();
$seats = $obj->index();

$obj_polling= new Polling();
$pollinges = $obj_polling->index();
include 'partials/header.php'; 
?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Center Information</h1>
            <a href="center_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Center</a>
          </div>
          <div class="row justify-content-center" style="margin-bottom: 20px;">
            <div class="col-sm-8">
              <div class="card">
              <div class="card-body">

            <form method="POST" id="cenderForm" class="user" action="../../app/center/Store.php" >

	         <div class="form-group row">
	              <label for="center_name" class="col-sm-4">Center Name </label>
	              <div class="col-sm-8">
	                  <input type="text" class="form-control " name="center_name" id="center_name " placeholder="Enter Seat Name " value="" required>
	              </div>
	          </div>

	          <div class="form-group row">
	              <label for="center_seat_id" class="col-sm-4">Seat Name </label>
	              <div class="col-sm-8">
	                  <select name="center_seat_id" id="center_seat_id" class="form-control">
	                      <option value="0" disabled="true" selected="true">===Select Seat Name===</option>
	                         <?php foreach ($seats as $seat) { ?>
	                         <option value="<?php echo $seat->seat_id; ?>" ><?php echo $seat->seat_name; ?></option>
	                         <?php } ?>
	                          
	                         
	                  </select>
	              </div>
	          </div>

            <div class="form-group row">
                <label for="center_polling_id" class="col-sm-4">Polling Officer Name </label>
                <div class="col-sm-8">
                    <select name="center_polling_id" id="center_polling_id" class="form-control">
                        <option value="0" disabled="true" selected="true">===Select Seat Name===</option>
                           <?php foreach ($pollinges as $polling) { ?>
                           <option value="<?php echo $polling->polling_id; ?>" ><?php echo $polling->polling_name; ?></option>
                           <?php } ?>
                            
                           
                    </select>
                </div>
            </div>

	         
	           <div class="form-group row">
              <label for="center_address" class="col-sm-4">Center Address</label>
              <div class="col-sm-8">
                  <textarea name="center_address" cols="4" rows="5" value="" class="form-control" id="center_address" required ></textarea>
                 
              </div>
          </div>

          <button class="btn btn-primary" type="submit">Create</button>
      </form>
       </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>

 <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script>
               $(document).ready(function () {
                   $.validator.setDefaults({
                       errorClass: "textstyle py-2 pb-1",
                       highlight: function (element, errorClass) {
                           $(element).removeClass(errorClass);
                       },
                       errorPlacement: function (error, element) {
                           error.insertAfter($(element).parent('div'));
                       }
                      
                   });
                   

                   $("#cenderForm").validate({

                       errorElement: 'div',
                       rules: {
                           canter_name: {
                               required: true,
                              
                           },
                            center_seat_id: {
                               required: true,
                           },
                            center_polling_id: {
                               required: true,
                               
                           },
                            center_address: {
                               required: true,
                           }
                       },
                       messages: {
                           
                           canter_name: {
                               required: "Enter Your Name"
                           },
                           center_seat_id: {
                               required: "Select Seat Name"
                           },
                            center_polling_id: {
                               required:"Select Polling Name"
                               
                           }
                           ,
                            center_address: {
                               required:"Enput Center Address"
                               
                           }
                       }
                   });
               });
           </script>

 
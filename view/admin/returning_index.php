<?php 


include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();

use App\User\Auth;
use App\Returning\Returning;
use App\Seat\Seat;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}

//for Auth Object Create
$auth= new Auth();
$status = $auth->setdata($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
//for Returning object create
$obj= new Returning();
$returnings = $obj->index();


$obj_seat= new Seat();


$i= 1;
include 'partials/header.php'; 
?>


<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <?php
      include 'partials/sidebar.php' 
       ?>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">All Returning Officers Information</h1>
            <a href="returning_create.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Add Returning</a>
          </div>

          <!-- Content Row -->
          <div class="row">

            <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Sl</th>
                <th>Name</th>
              
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($returnings as $returning) { ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $returning->returning_name; ?></td>  
                <td>
                  <a href="returning_edit.php?returning_id=<?php echo $returning->returning_id; ?>" class="btn btn-warning btn-sm">Edit</a>
                  <a href="../../app/returning/Delete.php?returning_id=<?php echo $returning->returning_id; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure!')">Delete</a>
                </td>
              </tr>

              <?php } ?>
            </tbody>
          </table>

          
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
<?php 
 include 'partials/footer.php'; 
?>
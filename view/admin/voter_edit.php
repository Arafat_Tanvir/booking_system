<?php 

include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Voter\Voter;
use App\Center\Center;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}

$seat = new Center();
$seats = $seat->index();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
$obj= new Voter;
$obj->setData($_GET);
$voter = $obj->edit();
include 'partials/header.php'; 
?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Voter Information</h1>
            <a href="voter_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Voter</a>
          </div>
          <div class="row" style="margin-bottom: 20px;">
            <div class="col-sm-8">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" action="../../app/voter/Update.php" enctype="multipart/form-data">
         <input type="hidden" name="voter_id" value="<?php echo $voter->voter_id; ?>">
         <div class="form-group row">
              <label for="voter_name" class="col-sm-4">Voter Name </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control " name="voter_name" id="voter_name " placeholder="Enter Voter Name " value=" <?php echo $voter->voter_name; ?>">
              </div>
          </div>
                <div class="form-group row">
                    <label for="voter_national_id" class="col-sm-4">Center</label>
                    <div class="col-sm-8">
                        <select name="center_id" id="center_id" class="form-control">
                            <option value="">Select Center</option>
                            <?php foreach ($seats as $centerdata) {?>
                                <option value="<?= $centerdata->center_id ?>" <?= $voter->center_id==$centerdata->center_id ? 'selected': ''?>><?= $centerdata->center_name?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
          <div class="form-group row">
              <label for="voter_father_name" class="col-sm-4">Voter Father Name </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control " name ="voter_father_name" id="voter_father_name " placeholder="Enter Father Voter Name " value="<?php echo $voter->voter_father_name; ?>">
              </div>
          </div>

          <div class="form-group row">
              <label for="voter_mother_name " class="col-sm-4">Voter Mother Name </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name ="voter_mother_name" id="voter_mother_name " placeholder="Enter Mother Voter Name " value="<?php echo $voter->voter_mother_name; ?>">
              </div>
          </div>

          <div class="form-group row">
              <label for="voter_gender" class="col-sm-4">Voter Gender</label>
              <div class="col-sm-8">
                  <div class="row justify-content-center">
                      <div class="col-sm-6">
                          <input type="radio" name="voter_gender" value="Male" <?php if ($voter->voter_gender=='Male') echo 'checked' ?> >Male
                      </div>
                      <label class="col-sm-6 ">
                           <input type="radio" name="voter_gender" value="Female" <?php if ($voter->voter_gender=='Female') echo 'checked'; ?> >Female
                      </label>
                  </div>
              </div>
          </div>

          
          <div class="form-group row">
              <label for="voter_date_of_birth " class="col-sm-4">Voter Date Of Birth </label>
              <div class="col-sm-8">
                  <input type="date" class="form-control " name ="voter_date_of_birth" id="voter_date_of_birth "  value="<?php echo $voter->voter_date_of_birth; ?>">
              </div>
          </div>

         

          <div class="form-group row">
              <label for="voter_address" class="col-sm-4">Voter Address</label>
              <div class="col-sm-8">
                  <textarea name="voter_address" cols="4" rows="5" value="" class="form-control" id="voter_address"><?php echo $voter->voter_address; ?></textarea>
                 
              </div>
          </div>


          <div class="form-group row">
              <label for="voter_image" class="col-sm-4">Voter Image</label>
              <div class="col-sm-8">
                  <input type="file" class="form-control " name="voter_image"  id="voter_image"  placeholder="Enter Your Image">
              </div>
          </div>


          <button class="btn btn-primary" type="submit">Create</button>
      </form>
       </div>
       </div>
              </div>
              <div class="col-sm-4">
              	<div class="card">
              		<div class="card-header">
              			<img src="<?php echo $voter->voter_image; ?>" class="img-responsive"  width="100%" >
              		</div>
              	</div>
              </div>
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>
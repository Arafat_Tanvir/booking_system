<?php 

include '../../vendor/autoload.php';
if(!isset($_SESSION) )session_start();
use App\User\Auth;
use App\Candidate\Candidate;
use App\Seat\Seat;

if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login.php");
}
$obj= new Candidate();
$obj->setData($_GET);
$candidate = $obj->edit();

//seat

$obj= new Seat();
$seats = $obj->index();

include 'partials/header.php'; 

?>
 <body id="page-top">
  <div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <?php
      include 'partials/sidebar.php' 
       ?>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <?php 
          include 'partials/navbar.php'
           ?>
        </nav>
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Center Information</h1>
            <a href="candidate_index.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Show Center</a>
          </div>
          <div class="row" style="margin-bottom: 20px;">
            <div class="col-sm-8 ">
              <div class="card">
              <div class="card-body">

            <form method="POST" class="user" action="../../app/candidate/Update.php" enctype="multipart/form-data">
         <input type="hidden" name="candidate_id" value="<?php echo $candidate->candidate_id; ?>">
         <div class="form-group row">
              <label for="candidate_name" class="col-sm-4">Center Name </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control " name="candidate_name" id="candidate_name " placeholder="Enter candidate Name " value=" <?php echo $candidate->candidate_name; ?>">
              </div>
          </div>

           <div class="form-group row">
              <label for="candidate_education" class="col-sm-4">Candidate Education </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control " name="candidate_education" id="candidate_education " placeholder="Enter candidate Name " value=" <?php echo $candidate->candidate_education; ?>">
              </div>
          </div>

          <div class="form-group row">
              <label for="candidate_gender" class="col-sm-4">Candidate Gender</label>
              <div class="col-sm-8">
                  <div class="row justify-content-center">
                      <div class="col-sm-6">
                          <input type="radio" name="candidate_gender" value="Male" <?php if ($candidate->candidate_gender=='Male') echo 'checked' ?> >Male
                      </div>
                      <label class="col-sm-6 ">
                           <input type="radio" name="candidate_gender" value="Female" <?php if ($candidate->candidate_gender=='Female') echo 'checked'; ?> >Female
                      </label>
                  </div>
              </div>
          </div>

          <div class="form-group row">
	              <label for="candidate_seat_id" class="col-sm-4">Seat Name </label>
	              <div class="col-sm-8">
	                  <select name="candidate_seat_id" id="candidate_seat_id" class="form-control">
	                      <option value="0" disabled="true" selected="true">===Select Seat Name===</option>
	                         <?php foreach ($seats as $seat) { ?>
	                         <option value="<?php echo $seat->seat_id; ?>" <?php if($seat->seat_id==$candidate->candidate_seat_id) echo 'selected'; ?>><?php echo $seat->seat_name; ?></option>
	                         <?php } ?>
	                          
	                         
	                  </select>
	              </div>
	          </div>

            <div class="form-group row">
                <label for="candidate_protik" class="col-sm-4">Candidate Protik</label>
                <div class="col-sm-8">
                    <input type="file" class="form-control " name="candidate_protik"  id="candidate_protik"  placeholder="Enter Ward Bangla Name" value="">
                </div>
            </div>

	         
	           <div class="form-group row">
              <label for="candidate_address" class="col-sm-4">Center Address</label>
              <div class="col-sm-8">
                  <textarea name="candidate_address" cols="4" rows="5" value="" class="form-control" id="candidate_address"><?php echo $candidate->candidate_address; ?></textarea>
                 
              </div>
          </div>

          <div class="form-group row">
              <label for="candidate_image" class="col-sm-4">Candidate Image</label>
              <div class="col-sm-8">
                  <input type="file" class="form-control " name="candidate_image"  id="candidate_image"  placeholder="Enter Ward Bangla Name" value="">
              </div>
          </div>
          <button class="btn btn-primary float-right" type="submit">Update</button>
      </form>
       </div>
       </div>
              </div>
               <div class="col-sm-4">
                <div class="card">
                  <div class="card-header">
                    <h4>Candidate Image</h4>
                    <hr>
                    <img src="<?php echo $candidate->candidate_image; ?>" class="img-responsive"  width="100%" height="220px" >
                  </div>
                </div>
                <div class="card" style="margin-top: 20px">
                  <div class="card-header">
                    <h4>Candidate Protik</h4>
                    <hr>
                    <img src="<?php echo $candidate->candidate_protik; ?>" class="img-responsive"  width="100%" height="120px" >
                  </div>
                </div>
               </div>
          </div>
        </div>
      </div>
<?php 
   include 'partials/footer.php'; 
 ?>
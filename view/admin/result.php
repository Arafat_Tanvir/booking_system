<?php 


include '../../vendor/autoload.php';
use App\User\Auth;
use App\Voting\Voting;
use App\Candidate\Candidate;

if(!isset($_SESSION) )session_start();
if($_SESSION['type']=='admin'){

}else{
  header("location:../login_new.php");
}
$obj_candidate=new Candidate();
$candidates=$obj_candidate->index();


$obj_vote=new Voting();
foreach ($candidates as $candidate) {
     $votes[]=count($obj_vote->get_all_vote_submited_BycandidateId($candidate->candidate_id));
}
if (!empty($votes)) {
  $won=max($votes);
}

// $id=$max[0][0]->candidate_id;

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
if(!$status) {
    header("location:../login_new.php");
}

$sl=1;
include 'partials/header.php'; 
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <?php
      include 'partials/sidebar.php' 
       ?>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <?php 
          include 'partials/navbar.php'
           ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
          </div>

          <!-- Content Row -->
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h4>National Election</h4>
                </div>
                <div class="card-body">
                   <table class="table table-bordered table-striped">
            <thead style="text-align: center;">
              <tr>
                <th>Protik</th>
                <th>Name</th>
                <th style="font-style: italic;">Image</th>
                
                <th>Result/Total Votes</th>
              </tr>
            </thead>
            <tbody style="text-align: center;">
              <?php foreach($candidates as $candidate) { ?>
              <tr style="background-color:<?php if(count($obj_vote->get_all_vote_submited_BycandidateId($candidate->candidate_id))==$won) echo 'pink'; ?>">
                <td><img src="<?php echo $candidate->candidate_protik; ?>" class="img-responsive" width="100"></td>
                <td><?php echo $candidate->candidate_name; ?></td>
                <td><img src="<?php echo $candidate->candidate_image; ?>" class="img-responsive" width="100"></td>
                 <td>
                  <?php echo  count($obj_vote->get_all_vote_submited_BycandidateId($candidate->candidate_id)); ?> Votes <?php if(count($obj_vote->get_all_vote_submited_BycandidateId($candidate->candidate_id))==$won) echo '(Win the election)'; ?>
                </td>
              </tr>

              <?php } ?>
            </tbody>
          </table>
                </div>
              </div>
            </div> 
            
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
       <?php 
           include 'partials/footer.php'; 
         ?>